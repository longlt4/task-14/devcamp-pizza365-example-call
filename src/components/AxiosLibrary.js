import { Component } from "react";

import axios from "axios";

class AxiosLibrary extends Component {

    axiosLibrary = async (url, body) => {
        let response = await axios(url, body);

        return response.data;
    }

    getAllAPI = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }

    getByIDAPI = () => {
        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders/kjAo2e1vw1")
            .then((data) => {
                console.log(data);
            })
    }

    postAPI = () => {
        let body = {
            body: ({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Teo",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        };

        axios.post("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data.data);
            })
    }

    updateByIDAPI = () => {
        let body = {
                hoTen: "Phạm Thanh Long",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza ko đế",
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        };

        axios.put("http://42.115.221.44:8080/devcamp-pizza365/orders/438501", body)
            .then((data) => {
                console.log(data);
            })
    }

    deleteByAPI = () => {
        let body = {
            method: 'DELETE',
        };

        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders/438495", body)
            .then((data) => {
                console.log(data);
            })
    }

    render() {
        return (
            <div className="row mt-2">
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getAllAPI}>Get ALL API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getByIDAPI} >Get by ID API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.postAPI}>Post API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.updateByIDAPI}>Update by ID API</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.deleteByAPI}>Delete by ID API</button>
                </div>
            </div>
        )
    }
}

export default AxiosLibrary;
