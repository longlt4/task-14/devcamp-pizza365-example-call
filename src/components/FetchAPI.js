import { Component } from "react";
class FetchAPI extends Component {
 fetchAPI = async (url, body) => {
    let response = await fetch(url, body);
    let data = await response.json();

    return data;
 }

 getAllAPI = () => {
    this.fetchAPI ("http://42.115.221.44:8080/devcamp-pizza365/orders")
    .then((data) => {
        console.log("alo")
        console.log(data);
    })
 }

 getByIDAPI = () => {
    this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/kjAo2e1vw1")
        .then((data) => {
            console.log(data);
        })
}

postAPI = () => {
    let body = {
        method: 'POST',
        body: JSON.stringify({
            kichCo: "M",
            duongKinh: "25",
            suon: "4",
            salad: "300",
            loaiPizza: "HAWAII",
            idVourcher: "16512",
            idLoaiNuocUong: "PEPSI",
            soLuongNuoc: "3",
            hoTen: "Phạm Thanh Bình",
            thanhTien: "200000",
            email: "binhpt001@devcamp.edu.vn",
            soDienThoai: "0865241654",
            diaChi: "Hà Nội",
            loiNhan: "Pizza đế dày"
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
    };

    this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
        .then((data) => {
            console.log(data);
        })
}

updateByIDAPI = () => {
    let body = {
        method: 'PUT',
        body: JSON.stringify({
            orderId: "mLfrj9o5oK",
            hoTen: "Phạm Thanh Long",
            thanhTien: "200000",
            email: "binhpt001@devcamp.edu.vn",
            soDienThoai: "0865241654",
            diaChi: "Hà Nội",
            loiNhan: "Pizza ko đế"
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
    };

    this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/438501", body)
        .then((data) => {
            console.log(data);
        })
}

deleteByAPI = () => {
    let body = {
        method: 'DELETE',
    };

    this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/438495", body)
        .then((data) => {
            console.log(data);
        })
}



render(){
    return(
        <div className="jumbotron row mt-2 center">
        <div className="col-2">
            <button className="btn btn-info" onClick={this.getAllAPI}>Get ALL API</button>
        </div>
        <div className="col-2">
            <button className="btn btn-success" onClick={this.getByIDAPI} >Get by ID API</button>
        </div>
        <div className="col-2">
            <button className="btn btn-primary" onClick={this.postAPI}>Post API</button>
        </div>
        <div className="col-2">
            <button className="btn btn-warning" onClick={this.updateByIDAPI}>Update by ID API</button>
        </div>
        <div className="col-2">
            <button className="btn btn-danger" onClick={this.deleteByAPI}>Delete by ID API</button>
        </div>
    </div>

    )
};
}
export default FetchAPI;